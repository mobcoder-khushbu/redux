import React from 'react';
import SoftContainer from './components/softContainer';
import {Provider} from 'react-redux';
import store from './components/redux/store';
import './App.css';

function App() {
  return (
    <Provider store={store}>
    <div className="App">
    <div className="App-header">
      <SoftContainer />
      </div>
    </div>
    </Provider>
  );
}

export default App;
