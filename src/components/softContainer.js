import React,{useState} from 'react';
import {connect} from 'react-redux';
import {buySoft} from '../components/redux';
function SoftContainer(props) {
    const [number, setNumber] = useState()
  return (
    <div className="App">
    <div className="App-logo">
    <img src={require('./Images/ab.png')} className="imageLogo"/>
    </div>
    <h3>No. of Software - {props.noOfSoftware}</h3>
    <input type="text" value={number} onChange={e=>setNumber(e.target.value)}/>
    <button onClick={()=>props.buySoft(number)}>Buy</button>
    </div>
  );
}
const mapStatetoProps=(state)=>{
    return {
        noOfSoftware:state.noOfSoftware
    }
}

const mapDispatchtoProps=(dispatch)=>{
    return{
        buySoft:function(number){
            dispatch(buySoft(number));
        }
    }
}

export default connect(mapStatetoProps,mapDispatchtoProps)(SoftContainer);
