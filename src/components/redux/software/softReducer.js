import {BUY_SOFT} from './softType';
const initialState={
    noOfSoftware:10
}

const softReducer=(state=initialState,action)=>{

    switch (action.type) {
        case BUY_SOFT:return{
            ...state,
            noOfSoftware:state.noOfSoftware-action.payload,
        }
            break;
    
        default:return state;
            break;
    }
}
    export default softReducer;