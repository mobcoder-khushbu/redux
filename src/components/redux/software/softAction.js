import {BUY_SOFT} from './softType';

export const buySoft=(number)=>{
    return {
        type:BUY_SOFT,
        payload:number
    }
}