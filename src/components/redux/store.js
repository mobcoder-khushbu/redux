import {createStore,applyMiddleware} from 'redux';
import logger from 'redux-logger'
import softReducer from './software/softReducer';

const store=createStore(softReducer,applyMiddleware(logger));

export default store;